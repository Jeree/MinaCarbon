
package CaminoMinimoCarbon;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;

import MinaCarbon.Mena;
import MinaCarbon.MinaCarbon;



public class MenacercanaconCarb
{
	
	
	public static ArrayList<Integer> resolverCaminoMinimoCarbon(MinaCarbon Mina, int menaInicio)
	{
	
		if(Mina.cantidadDeCarbon(menaInicio)>0)
			throw new IllegalArgumentException("la Mena (" + menaInicio + ") que useted consulto tiene carbon!");
	   
		//creo un arreglo de booleans para cada Mena de la mina y poner en true los nodos ya visitados
		boolean [] menaVisitada = new boolean [Mina.menas()];
		//creo un areglo de ints para cada mena y le asigno un valor muy alto, 
		//que va a ser reemplazado por la distancia delos tuneles de esa mena, dejando simepre la menor
		int [] tunelmasCortodeMena = new int [Mina.menas()];
		//creo un arreglo de int donde asigno de donde vengo(padre de la mena)
	
		boolean encontreCarbon=false;
		ArrayList<Integer> caminoArecorrer =new ArrayList<Integer>();
		iniciarParametrosControl(menaVisitada, tunelmasCortodeMena, Mina );
		Mena menaActual = Mina.devolverMena(menaInicio);
		int nombreMenaActual=menaInicio;
		int candidatoMinimo =0;
		int nombreAconsultar=0;
		
	
		
		while(todosVistos(menaVisitada)!=true && encontreCarbon==false)	
		{
				
				menaVisitada[nombreMenaActual]=true;
				if (menaActual.getCantCarbon()>0)
				{
					encontreCarbon=true;
					caminoArecorrer.add(nombreMenaActual);
					return caminoArecorrer;
				}
				else 
				{
					for(int l=0; l<menaActual.gettuneles().size();l++)
					{
						//Mena menaAconsultar=
						 nombreAconsultar=menaActual.gettuneles().get(l);
						if(menaVisitada[nombreAconsultar]==false)
						{
							int distancia= Mina.distanciaTunel(nombreMenaActual, nombreAconsultar);
							if (distancia<tunelmasCortodeMena[nombreMenaActual])
							{
								tunelmasCortodeMena[nombreMenaActual]=distancia;
								candidatoMinimo =nombreAconsultar;	
							}		
						}	
						
					}
					caminoArecorrer.add(nombreMenaActual);
					menaActual =Mina.devolverMena(candidatoMinimo);
					nombreMenaActual=candidatoMinimo;
					
				}
		}
	
		if (todosVistos(menaVisitada)==true && encontreCarbon==false)
			throw new IllegalArgumentException("Se recorrio toda la Mina que useted consulto y no se encontro carbon!");
		return caminoArecorrer;	
	}

	
	
	
	private static boolean todosVistos(boolean[] menaVisitada)
	{  int cont=0;
		for (int i=0; i<menaVisitada.length; i++)
			if(menaVisitada[i]==true)
				cont++;
		if (cont==menaVisitada.length)
			return true;
		else return false;
	}
	
	private static void iniciarParametrosControl(boolean [] menaVisitada, int [] tunelmasCortodeMena,MinaCarbon _Mina )
	{
		for (int i=0; i<_Mina.menas(); i++)
		{
			menaVisitada[i]=false;
			tunelmasCortodeMena[i]=999999999;
			
		}
	}
}

		
	/*	
		ArrayList<Integer> caminoArecorrer =  new ArrayList <Integer>();
		ArrayList<Integer> yaRecorrido =  new ArrayList <Integer>();
		
		recorrerSig(caminoArecorrer, yaRecorrido, _Mina, menaInicio);
		
		if(yaRecorrido.size()>_Mina.menas())
			 throw new IllegalArgumentException("Se recorrio toda la Mina que useted consulto y no se encontro carbon!");
		return null;
	
	}
	//tengo que chequiear que no vuelva a la mena de la que viene?
	//es un grafo dirigido, por lo tanto no deberia pode rvolver por su mismo camino
	//capas igaulk tenga que controlas las que ya recorri.
	//ahora tiene una array que guarda las ,mesas ya recorridas
	private static ArrayList<Integer> recorrerSig(ArrayList<Integer> caminoArecorrer,ArrayList<Integer> yaRecorrido, MinaCarbon _Mina, int menaInicio )
	{
		Mena puntoInicio = _Mina.devolverMena(menaInicio);
		int masCercano =0;
		int candDistancia=9999999;
		if(yaRecorrido.size()==_Mina.menas())
			 throw new IllegalArgumentException("Se recorrio toda la Mina que useted consulto y no se encontro carbon!");
		
		for(Integer i=0; i<puntoInicio.gettuneles().size(); i++)
		{
			if(!yaRecorrido.contains(i))
			{
					if(_Mina.cantidadDeCarbon(i)>0)
					{
						caminoArecorrer.add(i);
						return caminoArecorrer;
					}
					else if(i!=menaInicio)
					{
					 if(_Mina.distanciaTunel(i, menaInicio)>candDistancia)
					 	{
						masCercano=i;
						candDistancia=_Mina.distanciaTunel(i, menaInicio);
					 	}
					}
			}
		}
		caminoArecorrer.add(masCercano);
		yaRecorrido.add(masCercano);
		return recorrerSig( caminoArecorrer, yaRecorrido, _Mina, masCercano) ;
	}

	
	
	
		
		
		
		
*
*
*/
