package MinaCarbon;

import java.util.ArrayList;

public class Mena 
{
	int nombre;
	int CantCarbon;
	ArrayList <Integer> tuneles;
	
	public Mena(int i, int j , ArrayList<Integer> k)
	{
		this.nombre= i;
		this.CantCarbon=j;
		this.tuneles=k;
	}
	
	public int getNombre()
	{
		return this.nombre;
	}
	
	public  int getCantCarbon()
	{
		return this.CantCarbon;
	}
	public  ArrayList<Integer> gettuneles()
	{
	
		return this.tuneles;		
		
	}
	
	public void addTuneles( int destino)
	{
		this.tuneles.add(destino);
		 
	}
	
	
	public void setCarbon(int cantcarbon)
	{
		this.CantCarbon= cantcarbon;
	}

	
	
	public boolean contieneNombre(int mena) 

	{
		if(this.nombre!=mena)
			return false;
		else return true;
	}

	
}
