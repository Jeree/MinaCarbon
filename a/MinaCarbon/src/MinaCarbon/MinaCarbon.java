
package MinaCarbon;
import java.util.ArrayList;

	public class MinaCarbon
		{
			private int[][] _distancia;
		
			// Representamos una Mina por listas menas de carbon
			private ArrayList<Mena> _Mina;
			
			// Una mina de carbon se construye con todas sus menas aisladas
			public MinaCarbon(int menas)
			{
				_distancia= new int[menas][menas];
				_Mina = new ArrayList<Mena>(menas);
				for(int i=0; i<menas; ++i)
					//se crea una mena donde el nombre esta asignado por numero
					//el carbon se incia en 0 y no tiene tuneles
					_Mina.add(new Mena(i, 0, new ArrayList<Integer>()));
				
			}

		
				
		// Agregado de una Tunel dirigido con distancia 		
		public void agregarTunel(int i, int j, int distancia)
		{
	
			chequearMenas(i, j);
			chequearQueNoExisteTunel(i, j);
	
			_Mina.get(i).addTuneles(j);
			_distancia[i][j]= distancia;
		}
		
		//devuelve la distancia entre (i,j) protesta si no existe el tunel
		public int distanciaTunel(int i, int j)
		{
			if (existeTunel(i, j))
				return(_distancia[i][j]);
			throw new IllegalArgumentException("Se consulto un tunel inexistente! i, j  ("+ i +"," +j+")" );
		}
				
		// Borrado de un tunel
		public void borrartunel(int i, int j)
		{

			chequearMenas(i, j);
			chequearQueExisteTunel(i, j);
	
			_Mina.get(i).tuneles.remove(j);
			
		}
				
		public void agregarCarbon(int mena, int cantcarbon)
		{
			existeMena(mena);
			_Mina.get(mena).setCarbon(cantcarbon);
		}
		
		public int cantidadDeCarbon(int mena)
		{
		
			existeMena(mena);
			return(_Mina.get(mena).CantCarbon);
			
		}
		
		public void sacarCarbon(int mena , int quitarcarbon)
		{
			existeMena(mena);
			_Mina.get(mena).CantCarbon=_Mina.get(mena).CantCarbon - quitarcarbon ;
			
		}
		//si ya existe la pisa 
		public void agregarMena(int nombre,int cantcarbon)
		{ 
			chequearNoExisteMena(nombre);
			_Mina.add(new Mena(nombre, cantcarbon, null));
			
		}
		public void borrarMena(int nombre)
		{
			chequearExisteMena(nombre);
			_Mina.remove(nombre);
		}
	
		// conecciones de una mena
		public  ArrayList<Integer> menasConectadas(int mena)
		{
			existeMena(mena);
			return _Mina.get(mena).gettuneles();
		}
				
		// Informa si existe el tunel con direccion (i,j)
		public boolean existeTunel(int i, int j)
		{
			chequearMenas(i, j);
			return _Mina.get(i).tuneles.contains(j);
		}
		
		
		// Chequea que (i,j) corresponda a un tunel posible
		private void chequearMenas(int i, int j)
		{
			if( i < 0 || i >= menas() )
				throw new IllegalArgumentException("Se intento utilizar un tunel con una Mena fuera de rango! i = " + i);
	
			if( j < 0 || j >= menas() )
				throw new IllegalArgumentException("Se intento utilizar un tunel con una Mena fuera de rango! j = " + j);
			
			if( i == j )
				throw new IllegalArgumentException("Se Se intento utilizar un tunel entre una Mena y sigo misma! i = j = " + i);
		}
	
		// Protesta si el tunel (i,j) existe
		private void chequearQueNoExisteTunel(int i, int j)
		{
			if( existeTunel(i, j) || existeTunel(j, i))
				throw new IllegalArgumentException("El tunel (" + i + ", " + j + ") ya existe!");
		}
		
		// Protesta si el tunel (i,j) no existe
		private void chequearQueExisteTunel(int i, int j)
		{
			if( !existeTunel(i, j) || !existeTunel(j, i))
				throw new IllegalArgumentException("El tunel (" + i + ", " + j+ ") no existe!");
		}
	
		// Protesta si la mena i no existe
		private boolean existeMena(int i)
		{
			if( i < 0 || i >= menas())
				throw new IllegalArgumentException("Se consulto una Mena inexistente! i = " + i);
			return true;
		}
		private void chequearNoExisteMena(int nombre) 
		{
			if(existeMena(nombre))
				throw new IllegalArgumentException("la Mena (" + nombre + ") ya existe!");
						
		}
		public void chequearExisteMena(int nombre)
		{
			if(!existeMena(nombre));
			throw new IllegalArgumentException("la Mena (" + nombre + ") no existe!");
		}
	

		// Cantidad de Menas de Carbon y tuneles
		public int menas()
		{
			return _Mina.size();
		}
		public int tunel()
		{
			int ret = 0;
			for(int i=0; i<menas(); ++i)
			for(int j=i+1; j<menas(); ++j) if(existeTunel(i,j))
				++ret;
			
			return ret;
		}
		// devuelve la MEna en posicion i
		public Mena devolverMena(int mena)
		{
			return(_Mina.get(mena));
			
		}
			
		
}
