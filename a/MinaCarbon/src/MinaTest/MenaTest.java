package MinaTest;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import MinaCarbon.Mena;
import MinaCarbon.MinaCarbon;


public class MenaTest {

	
	public static class Assert
	{
		public static void iguales ( int [] esperado , ArrayList <Integer> obtenido) 
		{
			assertEquals ( esperado.length , obtenido.size());
			
			for (Integer nombre : esperado)
				assertTrue(obtenido.contains(nombre));
		}
	}
	
	@Test
	public void tresTunelesConectadosTest()
	{
		MinaCarbon mina = estructura () ;
		Mena mena = mina.devolverMena(2);
		ArrayList<Integer> conectados = mena.gettuneles();
		Assert.iguales(new int [] {1,3,4} , conectados);	
	}
	
	@Test
	public void unTunelConectado () 
	{
		MinaCarbon mina = estructura () ;
		Mena mena = mina.devolverMena(0);
		ArrayList<Integer> conectados = mena.gettuneles();
		Assert.iguales(new int [] {1}, conectados);
	}

	@Test
	public void sinTunlesConectados () 
	{
		MinaCarbon mina = estructura () ;
		Mena mena = mina.devolverMena(5);
		ArrayList<Integer> conectados = mena.gettuneles();
		Assert.iguales(new int[]{}, conectados);
	}
	
	@Test
	public void cantidadMenasTest()
	{
		MinaCarbon mina = new MinaCarbon (5);
		assertEquals(5, mina.menas());
		
	}
	
	@Test 
	public void cambiarCantidadCarbonTest ()
	{
		MinaCarbon mina = estructura () ;
		Mena mena = mina.devolverMena(0);
		mena.setCarbon(5);
		Integer carbon = mena.getCantCarbon(); 
		assertTrue(carbon.equals(5));
	}
	
	public MinaCarbon estructura ()
	{
		MinaCarbon mina = new MinaCarbon (6);
		mina.agregarTunel(0, 1, 2);
		mina.agregarTunel(1, 2, 4);
		mina.agregarTunel(2, 3, 8);
		mina.agregarTunel(2, 1, 9);
		mina.agregarTunel(3, 4, 16);
		mina.agregarTunel(2, 4, 32);
		return mina;
	}
	
	
	
}
