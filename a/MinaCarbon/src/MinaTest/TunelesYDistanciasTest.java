package MinaTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import MinaCarbon.MinaCarbon;

public class TunelesYDistanciasTest 
{

	@Test(expected=IllegalArgumentException.class)
	public void tunelNegativoTest() 
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(-1, 2, 2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void tunelExcedidoTest ()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(6, 1, 2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void segundoTunelNegativoTest() 
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(1, -2, 2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void segundoTunelExcedidoTest ()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(1, 6, 2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void tunelesIgualesTest () 
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(4, 4, 2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void coneccionTunelExistentee ()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(0, 1, 16);
		mina.agregarTunel(0, 1, 16);
	}
	
	@Test
	public void comprobarTunelAgregadoTest ()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(1, 2, 4);
		assertTrue(mina.existeTunel(1, 2));
	}
		
	@Test
	public void tuneliniciaEnCeroTest () 
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(0, 2, 4);
		assertTrue(mina.existeTunel(0, 2));
	}
	
	@Test
	public void tunelMaximoTest ()
	{
		MinaCarbon mina = new MinaCarbon (6);
		mina.agregarTunel(4, 5 , 4);
		assertTrue(mina.existeTunel(4, 5));
	}
	

	@Test
	public void eliminarTunelExistenteTest ()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(0, 2, 4);
		//mina.borrartunel(0, 2);
		assertFalse(!mina.existeTunel(0, 2));
	}
	
	@Test 
	public void menasConTunelesTest()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(2, 4, 2);
		mina.agregarTunel(3, 1, 4);
		assertEquals(5, mina.menas());
	}
	
	@Test
	public void tunelesVaciosTest()
	{
		MinaCarbon mina = new MinaCarbon (5);
		assertEquals(0, mina.tunel());
	}
	
	@Test
	public void tunelesNoVaciosTest()
	{
		MinaCarbon mina = new MinaCarbon (5);
		mina.agregarTunel(2, 4, 2);
		mina.agregarTunel(0, 4, 4);
		
		assertEquals(2, mina.tunel());
	}
	
	@Test
	public void consultarDistanciaTest () 
	{
		MinaCarbon mina = inicializarDistancias();
		assertEquals(2, mina.distanciaTunel(0, 1), 1e-5);
	}
	
	@Test
	public void consultarDistanciaCruzadaTest ( )
	{
		MinaCarbon mina = inicializarDistancias();
		assertEquals(2,mina.distanciaTunel(0, 1) , 1e-5
				);
	}
	
	@Test
	public void distanciaCeroTest ( )
	{
		MinaCarbon mina = inicializarDistancias();
		assertEquals(0,mina.distanciaTunel(2, 3),1e-5);
	}
	
	public MinaCarbon inicializarDistancias () 
	{
		MinaCarbon mina = new MinaCarbon ( 4 );
		mina.agregarTunel(0, 1, 2);
		mina.agregarTunel(1, 2, 4);
		mina.agregarTunel(2, 3, 0);
		
		
		return mina;
	}
}
