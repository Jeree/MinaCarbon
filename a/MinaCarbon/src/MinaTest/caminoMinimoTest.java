package MinaTest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import CaminoMinimoCarbon.MenacercanaconCarb;
import MinaCarbon.Mena;
import MinaCarbon.MinaCarbon;
import MinaTest.MenaTest.Assert;

class CaminoMinimoTest {

	@Test(expected = IllegalArgumentException.class)
	public void minaSinCarbon()
	{
		MinaCarbon mina = estructura ();
		Mena menaInicio = mina.devolverMena(0);
		MenacercanaconCarb.resolverCaminoMinimoCarbon(mina, menaInicio.getNombre());
		
	}

	@Test
	public void minaCarbonElementoConsultado()
	{
		MinaCarbon mina = estructura ();
		Mena menaInicio = mina.devolverMena(0);
		menaInicio.setCarbon(15);
		MenacercanaconCarb.resolverCaminoMinimoCarbon(mina, menaInicio.getNombre());
		
	}
	@Test
	public void minaCarbonElementomaslejano()
	{
		MinaCarbon mina = estructura ();
		Mena menaInicio = mina.devolverMena(0);
		Mena menaConCarbon = mina.devolverMena(4);
		menaConCarbon.setCarbon(15);
		assertEquals(caminoCCultimoelemento(),MenacercanaconCarb.resolverCaminoMinimoCarbon(mina, menaInicio.getNombre()));
		
	}

	public void minaCarbonElementomaMedio()
	{
		MinaCarbon mina = estructura ();
		Mena menaInicio = mina.devolverMena(0);
		Mena menaConCarbon = mina.devolverMena(2);
		menaConCarbon.setCarbon(15);
		assertEquals(caminoCCelementomedio(),MenacercanaconCarb.resolverCaminoMinimoCarbon(mina, menaInicio.getNombre()));
		
	}
	
	public MinaCarbon estructura () 
	{
		MinaCarbon mina = new MinaCarbon (6);
		mina.agregarTunel(0, 1, 2);
		mina.agregarTunel(1, 2, 4);
		mina.agregarTunel(2, 3, 8);
		mina.agregarTunel(3, 4, 16);
		mina.agregarTunel(2, 4, 32);
		return mina;	
	}
	public ArrayList<Integer> caminoCCultimoelemento()
	{
		ArrayList<Integer> caminos = new ArrayList<Integer>();
		caminos.add(0);
		caminos.add(1);
		caminos.add(2);
		caminos.add(3);
		caminos.add(4);
		return 	caminos;
		
	}
	public ArrayList<Integer> caminoCCelementomedio()
	{
		ArrayList<Integer> caminos = new ArrayList<Integer>();
		caminos.add(0);
		caminos.add(1);
		caminos.add(2);
		return 	caminos;
		
	}

}
